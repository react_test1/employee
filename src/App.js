import { useState } from "react";
import InputText from "./components/InputText"
import ShowText from "./components/ShowText"


export default function App() {
    const defaultValue = { emp: '123456', name: 'สิริพรภา', lastname: 'แฝงนาคำ' };

    // NOTE: สร้าง State โดยมีค่าตั้งต้นเป็น { emp: '123456', name: 'สิริพรภา', lastname: 'แฝงนาคำ' };
    // NOTE: แค่ถือค่าที่ User พิมพ์ไว้ใน Input เฉยๆ
    const [data, setData] = useState(defaultValue)

    // TODO: 1. สร้าง State เป็นตัวแปร Array ว่างๆ 1 ตัว
    const [newData, setNewData] = useState([])

    const onValueChange = (event) => {
        const { name, value } = event.target;

        const newData = {
            ...data,
            [name]: value,
        };

        setData(newData);
    }

    const onAdd = () => {
        // TODO: 2. เพิ่ม Element เข้าไปใน Array
        const newArray = [...newData, data];
        // NOTE: 3. Add new Array
        setNewData(newArray);


        console.log(newArray);
    }

    return (
        <div>
            <h1>เพิ่มหนักงาน</h1>
            <h4>ลงทะเบียนรายการพนักงาน</h4>
            <InputText onChange={onValueChange} dataInput={data} addData={onAdd}></InputText>
            <h1>รายการพนักงาน</h1>
            <ShowText dataShow={newData}></ShowText>
        </div>
    )
}
