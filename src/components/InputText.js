
// NOTE: เรียก Component ที่สร้างมาใช้ (Field)
import InputField from "./InputField";

// NOTE: เจี๊ยบวันเกิดไม่ถูกและลืม Destucturing Object ออกมา
export default function InputText({ dataInput, addData, onChange }) {

    // NOTE: Object Destructuring.
    const { emp, name, lastname } = dataInput;

    return (
        <div style={{ display: 'flex', flexDirection: 'column', gap: '16px' }}>
            <InputField label='EMP: ' name='emp' id='emp' value={emp} onChange={onChange} />
            <InputField label='Name: ' name='name' id='name' value={name} onChange={onChange} />
            <InputField label='Lastname: ' name='lastname' id='lastname' value={lastname} onChange={onChange} />
            <button onClick={addData}>Add</button>
        </div>
    );
}