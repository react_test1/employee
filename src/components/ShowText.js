
export default function ShowText({ dataShow }) {

    const mapData = () => {
        return dataShow.map(element => {
            const { emp, name, lastname } = element;
            return (
                <li>{`${emp} ${name} ${lastname}`}</li>
            )
        })
    }
    return (
        <ul>
            {mapData()}
        </ul>
    )
}