export default function InputField({ label, name, id, value, onChange }) {
    return <div style={{ display: 'flex', flexDirection: 'column' }}>
        <label>{label}</label>
        <input id={id} name={name} value={value} onChange={onChange}></input>
    </div>
}